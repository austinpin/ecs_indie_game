﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Entities;

public class Data : MonoBehaviour {

    //contains all the resources for the game
    //an entityarchetype is an entity that contains this set of components
    //for easier spawning

    private static EntityManager entityManager;
    public Camera cam;

    [Header("Player")]
    public static EntityArchetype PlayerArchetype;

    /*
    [Header("Planet")]
    public Mesh unitSpawnerMesh;
    public Material unitSpawnerMaterial;
    public static EntityArchetype unitSpawnerArchetype;
    */

    public GameObject shipPrefab;

    [Header("highlight entity")]
    public static EntityArchetype highlightE;
    public Mesh highlightMesh;
    public Material highlightMaterial;

    [Header("pathfinder entity")]
    public static EntityArchetype PathFinderE;

    [Header("Meshes")]
    public Mesh shipMesh;
    public Mesh planetMesh;
    [Header("materials")]
    public Material team1;
    public Material team2;
    public Material team3;
    public Material team4;
    public Material team5;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    public static void Initialize()
    {
        entityManager = World.Active.GetOrCreateManager<EntityManager>();

        PlayerArchetype = entityManager.CreateArchetype(
            ComponentType.Create<PlayerInput>(),
            ComponentType.Create<Team>());

        /*
        unitSpawnerArchetype = entityManager.CreateArchetype(
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<Team>(),
            ComponentType.Create<Orbitable>(),
            ComponentType.Create<Orders>(),
            ComponentType.Create<Planet>(),
            ComponentType.Create<MeshInstanceRenderer>(),
            ComponentType.Create<UnitSpawner>());
            */

        highlightE = entityManager.CreateArchetype(
            ComponentType.Create<Position>(),
            ComponentType.Create<Rotation>(),
            ComponentType.Create<HighlightTag>(),
            ComponentType.Create<MeshInstanceRenderer>());

        PathFinderE = entityManager.CreateArchetype(
            ComponentType.Create<StarSystem>(),
            ComponentType.Create<StarDestination>(),
            ComponentType.Create<StarThrough>());
    }
}
