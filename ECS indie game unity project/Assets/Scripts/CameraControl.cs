﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float xAxis;
    public float yAxis;

    public Transform child;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Input.GetAxis("Vertical") * transform.forward*3.2f;
        transform.position += Input.GetAxis("Horizontal") * transform.right*3.2f;
        transform.position += Input.GetAxis("Ascend") * transform.up*3.2f;

        if (Input.GetMouseButton(2))
        {
            yAxis += Input.GetAxis("Mouse X") * 1.8f;
            xAxis -= Input.GetAxis("Mouse Y") * 1.8f;

            transform.eulerAngles = new Vector3(0, yAxis, 0);
            child.localEulerAngles = new Vector3(xAxis, 0, 0);
        }
    }
}
