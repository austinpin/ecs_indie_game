﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class OrbParentInitializer : MonoBehaviour
{

    //you cant assign entities in the inspector (for entities that are also gameobjects)
    //this script is here to assign them and then gets removed

    public int mode;
    public bool iniStar;
    public bool iniSystem;

    [Header("orbs")]
    public GameObjectEntity parentEntity;
    private ParentECom parentComponent;

    [Header("connector")]
    public GameObjectEntity target1;
    public GameObjectEntity target2;
    private StarConnectorCom starConnector;

    [Header("star group")]
    public GameObjectEntity starEntity;
    private StarGroupCom starGroup;

    [Header("star system")]
    public GameObjectEntity starEntity1;
    private StarSystemCom starSystem;

    // Start is called before the first frame update
    void Start()
    {
        if (mode == 0)
        {
            parentComponent = GetComponent<ParentECom>();
            ParentE newParent = parentComponent.Value;
            newParent.entity = parentEntity.Entity;
            parentComponent.Value = newParent;
        }
        if (mode == 1)
        {
            starConnector = GetComponent<StarConnectorCom>();
            StarConnector connect = starConnector.Value;
            connect.target1 = target1.Entity;
            connect.target2 = target2.Entity;
            starConnector.Value = connect;
        }
        if (iniStar)
        {
            starGroup = GetComponent<StarGroupCom>();
            StarGroup group = starGroup.Value;
            group.star = starEntity.Entity;
            starGroup.Value = group;
        }

        if (iniSystem)
        {
            starSystem = GetComponent<StarSystemCom>();
            StarSystem system = starSystem.Value;
            system.star = starEntity1.Entity;
            starSystem.Value = system;
        }

        Destroy(this);
    }
}
