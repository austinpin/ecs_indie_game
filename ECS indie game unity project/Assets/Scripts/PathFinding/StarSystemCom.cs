﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using System.Collections.Generic;

[System.Serializable]
public struct StarSystem : ISharedComponentData
{
    public Entity star;
}
public class StarSystemCom : SharedComponentDataWrapper<StarSystem> { }