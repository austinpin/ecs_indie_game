﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

public class PathFinderBakeSys : ComponentSystem
{

    //path finder system which bakes the traversal as entities using
    //dijkstra's algorithm
    //the entities need to know where they are currently, and where they are going (the final destination)
    //to know where to go next until they reach their end point
    //for larger maps change the term value (its for not crashing if it doesent stop)

        //complex system

    public static JobHandle PathFinderHandle;

    private bool doOnce = true;

    private struct Connectors
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<StarConnector> connectArray;
    }

    private struct StarGroups
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<StarGroup> groupArray;
    }

    [Inject] Connectors connects;
    [Inject] StarGroups groups;

    private struct PathFinderJob : IJobParallelFor
    {
        public int connectLength;
        [ReadOnly] public ComponentDataArray<StarConnector> connectArray;

        public int groupLength;
        [ReadOnly] public ComponentDataArray<StarGroup> groupArray;

        //will run each stargroup seperatly
        public void Execute(int index)
        {

        }
        //execute end
    }

    // Update is called once per frame
    protected override void OnUpdate()
    {
        if (doOnce)
        {
            PathFinderJob pathFinderJob = new PathFinderJob
            {
                connectLength = connects.Length,
                connectArray = connects.connectArray,
                groupLength = groups.Length,
                groupArray = groups.groupArray,
            };
            //PathFinderHandle = pathFinderJob.Schedule(groups.Length,1, inputDeps);

            BakePath();
            doOnce = false;
        }

        //return PathFinderHandle;
    }

    void BakePath()
    {
        for (int mainIndex = 0; mainIndex < groups.Length; mainIndex++)
        {
            NativeArray<StarGroup> newGroup = new NativeArray<StarGroup>(groups.Length, Allocator.Temp);
            groups.groupArray.CopyTo(newGroup, 0);

            //initialise groups
            for (int a = 0; a < groups.Length; a++)
            {
                StarGroup aGroup = newGroup[a];
                aGroup.stackedDistance = -1;
                newGroup[a] = aGroup;
            }

            //set current group as initial
            StarGroup currentGroup = newGroup[mainIndex];
            currentGroup.stackedDistance = 0;
            currentGroup.visted = 1;
            newGroup[mainIndex] = currentGroup;

            byte complete = 0;

            //algorithm
            while (complete == 0)
            {
                //for each connector
                for (int i = 0; i < connects.Length; i++)
                {
                    StarConnector connect = connects.connectArray[i];

                    //check if any end is the group we are at
                    if (connect.target1 == currentGroup.star)
                    {
                        //now check all the groups
                        for (int j = 0; j < groups.Length; j++)
                        {
                            StarGroup newCurrentGroup = newGroup[j];
                            //if we have not used this group as a current group
                            if (newCurrentGroup.visted != 1)
                            {
                                //check other end of connection of this connection
                                if (connect.target2 == newCurrentGroup.star)
                                {
                                    //if it does not have a distance
                                    if (newCurrentGroup.stackedDistance == -1)
                                    {
                                        newCurrentGroup.stackedDistance = connect.myDistance + currentGroup.stackedDistance;
                                        newCurrentGroup.shortestDistanceLastStar =currentGroup.star;
                                        newGroup[j] = newCurrentGroup;
                                    }

                                    //new reassign distrance
                                    float currentDist = connect.myDistance + currentGroup.stackedDistance;
                                    if (currentDist < newCurrentGroup.stackedDistance)
                                    {
                                        newCurrentGroup.stackedDistance = currentDist;
                                        newCurrentGroup.shortestDistanceLastStar = currentGroup.star;
                                        newGroup[j] = newCurrentGroup;
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    //same as above
                    if (connect.target2 == currentGroup.star)
                    {
                        for (int j = 0; j < groups.Length; j++)
                        {
                            StarGroup newCurrentGroup = newGroup[j];
                            if (newCurrentGroup.visted != 1)
                            {
                                if (connect.target1 == newCurrentGroup.star)
                                {
                                    if (newCurrentGroup.stackedDistance == -1)
                                    {
                                        newCurrentGroup.stackedDistance = connect.myDistance + currentGroup.stackedDistance;
                                        newCurrentGroup.shortestDistanceLastStar = currentGroup.star;
                                        newGroup[j] = newCurrentGroup;
                                    }

                                    float currentDist = connect.myDistance + currentGroup.stackedDistance;
                                    if (currentDist < newCurrentGroup.stackedDistance)
                                    {
                                        newCurrentGroup.stackedDistance = currentDist;
                                        newCurrentGroup.shortestDistanceLastStar = currentGroup.star;
                                        newGroup[j] = newCurrentGroup;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                //reset variables for this iteration
                float shortestDistance = -1;
                int shortestIndex = -1;
                //find new shortest group
                for (int b = 0; b < groups.Length; b++)
                {
                    //have not been there, have not checked its distance at all
                    if (newGroup[b].visted != 1 && newGroup[b].stackedDistance != -1)
                    {
                        if (shortestDistance == -1)//no data found so use any
                        {
                            shortestDistance = newGroup[b].stackedDistance;
                            shortestIndex = b;
                        }

                        if (newGroup[b].stackedDistance < shortestDistance)//data found so change if shorter
                        {
                            shortestDistance = newGroup[b].stackedDistance;
                            shortestIndex = b;
                        }
                    }
                }
                if (shortestIndex != -1)//we found a new current star, 
                {
                    currentGroup = newGroup[shortestIndex];
                    currentGroup.visted = 1;
                    newGroup[shortestIndex] = currentGroup;
                }
                else
                {
                    complete = 1;
                    //algorithm complete
                }
            }

            //algorithm complete
            //create pathfinder entities

            //use tracing mechanics to create a path
            for (int o = 0; o < groups.Length; o++)
            {
                if (newGroup[o].star==newGroup[mainIndex].star)
                {

                }
                else
                {
                    StarGroup currentStarGroup = newGroup[o];
                    StarGroup lastStarGroup;
                    int term = 0;
                    while (currentStarGroup.star != newGroup[mainIndex].star && term<5000) {
                        term++;
                        //find connected last star in tree
                        for (int z = 0; z < groups.Length; z++)
                        {
                            if (currentStarGroup.shortestDistanceLastStar == newGroup[z].star)
                            {
                                lastStarGroup = currentStarGroup;
                                currentStarGroup = newGroup[z];

                                if (currentStarGroup.star==newGroup[mainIndex].star)
                                {
                                    PostUpdateCommands.CreateEntity(Data.PathFinderE);
                                    PostUpdateCommands.SetSharedComponent(new StarSystem { star = newGroup[mainIndex].star });
                                    PostUpdateCommands.SetSharedComponent(new StarDestination { star = newGroup[o].star });
                                    PostUpdateCommands.SetSharedComponent(new StarThrough { star = lastStarGroup.star });
                                }

                                break;
                            }
                        }
                    }
                }
                //inside second iterator
            }
            //inside main index
        }
        //inside function
    }
    //inside class
}
