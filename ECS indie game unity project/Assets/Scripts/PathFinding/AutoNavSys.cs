﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

[UpdateAfter(typeof(OrderBarrier))]
[UpdateAfter(typeof(HyperTravelSystem))]
public class AutoNavSys : JobComponentSystem
{

    //before only the order system could move units
    //but with the creation of a path finder its important to auto move ships
    //this system fufils the same purpose as the inner order system
    //it moves ships closer to their final destination

    public static JobHandle autoNavHandle;

    private struct StarSystems
    {
        public readonly int Length;
        [ReadOnly] public SharedComponentDataArray<StarSystem> starSystem;
        [ReadOnly] public SharedComponentDataArray<StarThrough> starThrough;
        [ReadOnly] public SharedComponentDataArray<StarDestination> starDestination;
    }

    struct Systems
    {
        public readonly int Length;
        public EntityArray planet;
        [ReadOnly] public SharedComponentDataArray<StarSystem> system;
        [ReadOnly] public ComponentDataArray<Orbitable> orb;
    }

    [Inject] StarSystems systems;
    [Inject] Systems _system;

    //fix going to planet instead of star bug

    private struct AutoNavJob : IJobProcessComponentData<HyperTravel,Position,Orbiting,ParentE>
    {
        public int systemLength;
        [ReadOnly] public SharedComponentDataArray<StarSystem> starSystem;
        [ReadOnly] public SharedComponentDataArray<StarThrough> starThrough;
        [ReadOnly] public SharedComponentDataArray<StarDestination> starDestination;

        public int starLength;
        public EntityArray planet;
        [ReadOnly] public SharedComponentDataArray<StarSystem> system;
        [ReadOnly] public ComponentDataArray<Orbitable> orbitable;

        public void Execute(ref HyperTravel hyper,[ReadOnly] ref Position pos, ref Orbiting orb, ref ParentE parent)
        {
            if (hyper.finalDestination!=hyper.target)
            {//if i am at my destination, dont do anything
                if (hyper.target!=hyper.finalStar)
                {//if im not at my destiaton starsystem - get closer to there
                    if (hyper.cooldown <= 0)
                    {
                        for (int b = 0; b < starLength; b++)
                        {//check star systems
                            if (parent.entity == planet[b])
                            {//if i am orbiting this planet
                                for (int a = 0; a < systemLength; a++)
                                {//check baked pathfinder
                                    if (starDestination[a].star == hyper.finalStar)
                                    {
                                        if (starSystem[a].star == system[b].star)
                                        {
                                            hyper.target = starThrough[a].star;
                                        }
                                    }
                                }
                                float2 radiusOffset = new float2(0, orbitable[b].radius.y - orbitable[b].radius.x);
                                float myRadiusOffset = orb.myRadius - orbitable[b].radius.x;
                                hyper.offset = math.normalize(pos.Value - orb.parentPos);
                                hyper.percentOffset = math.unlerp(radiusOffset.x, radiusOffset.y, myRadiusOffset);
                            }
                        }

                        parent.entity = Entity.Null;
                        orb.active = 0;
                        hyper.cooldown = hyper.maxCooldown;
                        hyper.active = 1;
                    }
                }
                else
                {//if it is the final star - and my target is a planet, go to the planet
                    if (hyper.cooldown <= 0)
                    {
                        for (int b = 0; b < starLength; b++)
                        {//check star systems
                            if (parent.entity == planet[b])
                            {//if i am orbiting this planet
                                hyper.target = hyper.finalDestination;

                                float2 radiusOffset = new float2(0, orbitable[b].radius.y - orbitable[b].radius.x);
                                float myRadiusOffset = orb.myRadius - orbitable[b].radius.x;
                                hyper.offset = math.normalize(pos.Value - orb.parentPos);
                                hyper.percentOffset = math.unlerp(radiusOffset.x, radiusOffset.y, myRadiusOffset);

                                parent.entity = Entity.Null;
                                orb.active = 0;
                                hyper.cooldown = hyper.maxCooldown;
                                hyper.active = 1;
                            }
                        }
                    }
                }
            }
        }
        //execute end
    }

    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        AutoNavJob autoNavJob = new AutoNavJob
        {
            systemLength = systems.Length,
            starSystem = systems.starSystem,
            starThrough = systems.starThrough,
            starDestination = systems.starDestination,

            starLength = _system.Length,
            system = _system.system,
            orbitable=_system.orb,
            planet=_system.planet,
        };
        autoNavHandle = autoNavJob.Schedule(this, inputDeps);

        return autoNavHandle;
    }
    //inside class
}
