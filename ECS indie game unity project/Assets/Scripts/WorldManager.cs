﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class WorldManager : MonoBehaviour {

    //initialiser script
    //now redundant, it contains access to a data section for all resources

    [Header("data")]
    public static Data data;

    [Header("UI")]
    public Text FPSText;
    float deltaTime;
    int fps=0;

    public static EntityManager entityManager;
    public static EntityArchetype bulletArcheType;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void InitializeAfter()
    {
        entityManager = World.Active.GetOrCreateManager<EntityManager>();
    }

    void Start() {
        data = GetComponent<Data>();
        
        Entity playerEntity = entityManager.CreateEntity(Data.PlayerArchetype);
        entityManager.SetComponentData(playerEntity, new Team { id = 1 });

        Entity enemy2Entity = entityManager.CreateEntity(Data.PlayerArchetype);
        entityManager.SetComponentData(enemy2Entity, new Team { id = 2 });

        Entity enemy3Entity = entityManager.CreateEntity(Data.PlayerArchetype);
        entityManager.SetComponentData(enemy3Entity, new Team { id = 3 });

        Entity enemy4Entity = entityManager.CreateEntity(Data.PlayerArchetype);
        entityManager.SetComponentData(enemy4Entity, new Team { id = 4 });

        Entity enemy5Entity = entityManager.CreateEntity(Data.PlayerArchetype);
        entityManager.SetComponentData(enemy5Entity, new Team { id = 5 });
    }

    // calculates fps
    //fps here is the number of frames collected each second not the mode fps
    void Update () {
        deltaTime += Time.deltaTime;
        fps++;
        if (deltaTime >= 1)
        {
            deltaTime = 0;
            FPSText.text = fps.ToString();
            fps = 0;
        }

    }
}
