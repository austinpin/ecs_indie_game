﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using UnityEngine;

//[UpdateAfter(typeof(DeathSystem))]
//public class DeathBarrier : BarrierSystem { }

[UpdateAfter(typeof(ShipAttackSystem))]
public class DeathSystem : ComponentSystem
{

    //destroyes dead entities
    //without job due to bug

    public static JobHandle deathHandle;

    struct health
    {
        public readonly int Length;
        public EntityArray E;
        public ComponentDataArray<Health> _health;
    }
    

    [Inject] health h;
    //[Inject] private DeathBarrier deathBarrier;

    /*
    [BurstCompile]
    private struct DeathJob : IJobProcessComponentDataWithEntity<Health>
    {
        [ReadOnly]public EntityCommandBuffer buffer;
        public Entity _null;

        public void Execute(Entity E ,int index,[ReadOnly] ref Health health)
        {
            if (health.health <= 0)
            {
                buffer.DestroyEntity(E);
            }
        }
    }
    */
    

    //jobify this
    protected override void OnUpdate()
    {
        ShipAttackSystem.attackHandle.Complete();

        
        for(int i=0;i<h.Length;i++)
        {
            if(h._health[i].health<=0)
            {
                PostUpdateCommands.DestroyEntity(h.E[i]);
            }
        }
        

        /*
        DeathJob deathJob = new DeathJob
        {
            buffer = deathBarrier.CreateCommandBuffer(),
            _null = Entity.Null,
        };
        deathHandle = deathJob.Schedule(this, inputDeps);
        
        
        return deathHandle;
        */
    }
}
