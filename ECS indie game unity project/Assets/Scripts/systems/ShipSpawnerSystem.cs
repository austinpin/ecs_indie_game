﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Rendering;
using Unity.Burst;

[UpdateAfter(typeof(SpawnerSystem))]
[UpdateAfter(typeof(SpawnerBarrier))]
public class ShipSpawnerSystem : ComponentSystem
{

    //after spawner system
    //it actually spawns the ships
    //without jobs as this is how alpha features are

    struct players
    {
        public readonly int Length;
        public ComponentDataArray<PlayerInput> input;
    }

    struct Planets
    {
        public readonly int Length;
        public EntityArray PlanetE;
        [ReadOnly] public ComponentDataArray<Team> team;
        [ReadOnly] public ComponentDataArray<Position> position;
        public ComponentDataArray<Orbitable> orbitable;
        [ReadOnly] public ComponentDataArray<UnitSpawner> spawner;
    }

    [Inject] private players _players;
    [Inject] private Planets _planets;

    Unity.Mathematics.Random rand = new Unity.Mathematics.Random(12);

    //if you are reading this in the source change log //the outdated methods always get removed
    //THE
    //ships = new NativeArray<Entity>(totalToSpawn, Allocator.Temp);
    //EntityManager.Instantiate(ship, ships);
    //METHOD, is significantly faster than spawning each unit individual,
    //it converts a gameobject prefab, (as entityArchetype is not technicaly and entity to spawn, and entity does not contain component data)
    //so it grabs the gameobject from data, or worldmanager, (or it assigns it here)
    //essentialy spawning in bulk but not as pretty.
    //goes from 100MS to 15MS (100 units over 30 entities every 0.1s)
    //this is mostly due to the job not being in effect as entities are spawned on the main thread
    //spawning each entity and adding components is slower than spawning all the entities with the desired components and changing them.
    //as entities are moved around in memory as components are added and adding them with i guess simple machine code arrays is faster.

    protected override void OnUpdate()
    {
        SpawnerSystem.spawnerHandle.Complete();
        NativeArray<Entity> ships;

        int totalToSpawn = 0;
        int currentSpawned = 0;//incrementor for spawned ships 

        for (int h = 0; h < _players.Length; h++)
        {
            totalToSpawn += _players.input[h].spawnBatch;
        }
        if (totalToSpawn > 0)
        {

            //these 2 paragraphs are just to copy the arrays into a local temp storage
            //as creating entities with entity manager (the super fast method)
            //invalidated the injected arrays
            //also 2018.3 i think has a new feature where arrays (allocator.temp) are auto dumped for you
            //but allocator.temp native containers are still not burst compatable :/
            int length = _planets.Length;
            NativeArray<Entity> _planetE=new NativeArray<Entity>(length,Allocator.Temp);
            NativeArray<Team> _team =  new NativeArray<Team>(length, Allocator.Temp);
            NativeArray<Position> _position = new NativeArray<Position>(length, Allocator.Temp);
            NativeArray<Orbitable> _orbitable =  new NativeArray<Orbitable>(length, Allocator.Temp);
            NativeArray<UnitSpawner> _spawner = new NativeArray<UnitSpawner>(length, Allocator.Temp);

            _planets.PlanetE.CopyTo(_planetE,0);
            _planets.team.CopyTo(_team,0);
            _planets.position.CopyTo(_position,0);
            _planets.orbitable.CopyTo(_orbitable,0);
            _planets.spawner.CopyTo(_spawner,0);

            //create array of entities and full that up the data of this array with the spawned entities
            //use this entity data to set their components too
            ships = new NativeArray<Entity>(totalToSpawn, Allocator.Temp);
            EntityManager.Instantiate(WorldManager.data.shipPrefab, ships);

            //for each planet
            for (int i = 0; i < length; i++)
            {
                int currentAmmount = _spawner[i].ammountCurrent;
                if (currentAmmount>0) {
                    int _id = _team[i].id;
                    Material _current;
                    Entity self = _planetE[i];
                    Orbitable orbit = _orbitable[i];
                    Position position = _position[i];
                    float3 _pos;

                    //set material
                    if (_id == 1)
                    {
                        _current = WorldManager.data.team1;
                    }
                    else if (_id == 2)
                    {
                        _current = WorldManager.data.team2;
                    }
                    else if (_id == 3)
                    {
                        _current = WorldManager.data.team3;
                    }
                    else if (_id == 4)
                    {
                        _current = WorldManager.data.team4;
                    }
                    else
                    {
                        _current = WorldManager.data.team5;
                    }

                    //for each ship entity in the array up to this spawners range
                    for (int j = 0; j < currentAmmount; j++)
                    {
                        if (currentSpawned >= ships.Length)
                        {
                            //logic issue exists where index is higher than array
                            break;
                        }

                        float _rotation = rand.NextFloat(0, 360);
                        float _tilt = rand.NextFloat(0, 45);
                        float _radius = rand.NextFloat(orbit.radius.x, orbit.radius.y);

                        float3 pos = position.Value;

                        _pos.x = (math.sin(_rotation) * (math.cos(_tilt)) * _radius) + pos.x;
                        _pos.y = (math.sin(_rotation) * (math.sin(_tilt)) * _radius) + pos.y;
                        _pos.z = (math.cos(_rotation) * _radius) + pos.z;

                        //uses gameobeject prefab data to preassign data
                        //this is coustom data for each entity

                        EntityManager.SetComponentData(ships[currentSpawned], new Position { Value = _pos });
                        EntityManager.SetComponentData(ships[currentSpawned], new ParentE { entity = self });
                        EntityManager.SetComponentData(ships[currentSpawned], new Orbiting
                        {
                            active = 1,
                            parentPos = pos,
                            myRadius = _radius,
                            orbitSpeed = 0.6f,
                            rotation = _rotation,
                            tilt = _tilt
                        });
                        EntityManager.SetSharedComponentData(ships[currentSpawned], new MeshInstanceRenderer { mesh = WorldManager.data.shipMesh, material = _current });
                        EntityManager.SetComponentData(ships[currentSpawned], new Team { id = _id });
                        currentSpawned++;//next ship
                    }
                }

            }
            ships.Dispose(); //dispose of the native array we are not using it
            //now automatic for temp in a new version
        }
    }
}
