﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

public class CaptureBarrier : BarrierSystem { }

[UpdateAfter(typeof(ArrivalSystem))]
public class CaptureSystem : JobComponentSystem
{

    //allows nodes to be captured for any team

    public static JobHandle captureHandle;

    struct Ships
    {
        public readonly int Length;
        public EntityArray ships;
        [ReadOnly] public ComponentDataArray<ParentE> parent;
        [ReadOnly] public ComponentDataArray<Team> team;
    }

    struct planets
    {
        public readonly int Length;
        public EntityArray planetE;
        [ReadOnly] public ComponentDataArray<Orbitable> orbitable;
        [ReadOnly] public ComponentDataArray<Team> team;
    }

    [Inject] private Ships _ships;
    [Inject] private planets _planets;
    [Inject] CaptureBarrier barrier;

    [BurstCompile]
    private struct CaptureJob : IJobProcessComponentDataWithEntity<Orbitable, Team>
    {
        [ReadOnly] public ComponentDataArray<ParentE> _shipsParent;
        public EntityArray ships;
        [ReadOnly] public ComponentDataArray<Team> _shipsTeam;
        public int _shipsLength;

        public float deltaTime;

        //cant write to team as there are 2 copies of team (2 native arrays cannot be the same inside jobs)
        //its allowed if both are read only

        //for each planet
        public void Execute(Entity orbitE,int index,ref Orbitable orbit , [ReadOnly] ref Team team)
        {
            Team planetTeam = team;

            //for each ship
            for (int j = 0; j < _shipsLength; j++)
            {

                ParentE parent = _shipsParent[j];
                //if this ship is orbiting this planet
                if (parent.entity == orbitE)
                {
                    Entity e = ships[j];
                    Team shipTeam = _shipsTeam[j];

                    //set team
                    if (orbit.captureProgress <= 0)
                    {
                        planetTeam = shipTeam;
                    }

                    //thus so we can increment if its on our team
                    if (shipTeam.id == planetTeam.id)
                    {
                        orbit.captureProgress += deltaTime;
                    }
                    else if (shipTeam.id != planetTeam.id)
                    {
                        orbit.captureProgress -= deltaTime;
                    }
                    //store id as we cannot change this currently
                    orbit.teamIDStore = planetTeam.id;
                    //capture
                }
            }
            if (orbit.captureProgress >= orbit.captureLimit)
            {
                orbit.captureProgress = orbit.captureLimit;
                orbit.built = 1;
            }
            else if (orbit.captureProgress <= 0)
            {
                orbit.captureProgress = 0;
                orbit.built = 0;
                planetTeam.id = 0;
            }
            orbit.teamIDStore = planetTeam.id;
        }
    }

    /*
    struct UpdateTeamJob : IJobProcessComponentData<Orbitable,Team>
    {
        [ReadOnly]
        public EntityCommandBuffer buffer;

        public void Execute([ReadOnly]ref Orbitable orbitable,  ref Team team)
        {
            if (team.id!=orbitable.teamIDStore) {
                team.id = orbitable.teamIDStore;
                
            }
        }
    }
    */

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        EntityCommandBuffer buffer = barrier.CreateCommandBuffer();
        //JobHandle newhandle = JobHandle.CombineDependencies(inputDeps, ArrivalSystem.arrivalHandle);

        for (int i=0;i<_planets.Length;i++)
        {
            if (_planets.team[i].id!=_planets.orbitable[i].teamIDStore)
            {
                //change team id
                buffer.SetComponent(_planets.planetE[i],new Team {id= _planets.orbitable[i].teamIDStore });
                //change material
                if (_planets.orbitable[i].teamIDStore == 1)
                {
                    buffer.SetSharedComponent(_planets.planetE[i], new MeshInstanceRenderer { mesh = WorldManager.data.planetMesh, material = WorldManager.data.team1 });
                }
                if (_planets.orbitable[i].teamIDStore == 2)
                {
                    buffer.SetSharedComponent(_planets.planetE[i], new MeshInstanceRenderer { mesh = WorldManager.data.planetMesh, material = WorldManager.data.team2 });
                }
                if (_planets.orbitable[i].teamIDStore == 3)
                {
                    buffer.SetSharedComponent(_planets.planetE[i], new MeshInstanceRenderer { mesh = WorldManager.data.planetMesh, material = WorldManager.data.team3 });
                }
                if (_planets.orbitable[i].teamIDStore == 4)
                {
                    buffer.SetSharedComponent(_planets.planetE[i], new MeshInstanceRenderer { mesh = WorldManager.data.planetMesh, material = WorldManager.data.team4 });
                }
                if (_planets.orbitable[i].teamIDStore == 5)
                {
                    buffer.SetSharedComponent(_planets.planetE[i], new MeshInstanceRenderer { mesh = WorldManager.data.planetMesh, material = WorldManager.data.team5 });
                }
            }
        }

        CaptureJob captureJob = new CaptureJob
        {
            _shipsParent = _ships.parent,
            _shipsTeam = _ships.team,
            _shipsLength = _ships.Length,
            ships = _ships.ships,
            deltaTime = Time.deltaTime,
        };
        captureHandle = captureJob.Schedule(this, inputDeps);

        /*
        UpdateTeamJob updateTeamJob = new UpdateTeamJob
        {
            buffer = buffer,
        };
        //captureHandle = updateTeamJob.Schedule(this, newhandle);
        */

        return captureHandle;
    }
}
