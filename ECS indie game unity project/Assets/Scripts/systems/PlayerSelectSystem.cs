﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//public class PlayerSelectBarrier : BarrierSystem { }

public class PlayerSelectSystem : JobComponentSystem {

    //this system allows the player to select planets and send units

    struct Planets
    {
        public readonly int Length;
        [ReadOnly] public EntityArray Planet;
        [ReadOnly] public ComponentDataArray<Position> pos;
        [ReadOnly] public ComponentDataArray<Orbitable> orb;
        [ReadOnly] public SharedComponentDataArray<StarSystem> system;
        [ReadOnly] public ComponentDataArray<Team> team;
        public ComponentDataArray<Orders> orders;
    }

    struct PInput
    {
        public readonly int Length;
        public ComponentDataArray<PlayerInput> pInput;
    }

    [Inject] private Planets _planets;
    [Inject] private PInput pInput;
    //[Inject] private PlayerSelectBarrier playerSelectBarrier;

    Camera cam;

    public PlayerSelectSystem()
    {
        cam = Camera.main;
    }

    public static JobHandle PlayerSelectHandle;

    //thanks to (1)
    //https://answers.unity.com/questions/869869/method-of-finding-point-in-3d-space-that-is-exactl.html

    //[BurstCompile]
    struct PlayerSelectJob : IJobParallelFor
    {
        [ReadOnly] public EntityArray planet;
        [ReadOnly] public ComponentDataArray<Position> pos;
        [ReadOnly] public ComponentDataArray<Orbitable> orb;
        [ReadOnly] public SharedComponentDataArray<StarSystem> system;
        [ReadOnly] public ComponentDataArray<Team> team;
        public ComponentDataArray<Orders> orders;
        public float3 rayDirection;
        public float3 rayOrigin;
        public bool select;
        public bool send;

        [NativeDisableParallelForRestriction]
        public ComponentDataArray<PlayerInput> pInput;

        private Entity target;
        private bool over;
        private PlayerInput input;
        private Orders _order;

        public void Execute(int i)
        {
            //current object
            _order = orders[i];
            Orbitable newOrb = orb[i];
            float3 centreToStart = rayOrigin - pos[i].Value;

            //see (1) above for credits and explanation, not sure how this works but its a mathmatical formula for solving for a variable
            //i seen it in maths classes long ago in a different form for solving for x
            float a = math.dot(rayDirection, rayDirection);
            float b = 2 * math.dot(centreToStart, rayDirection); ;
            float c = math.dot(centreToStart, centreToStart) - ((orb[i].radius.y*2) * (orb[i].radius.y*2));

            //if the ray is over the object, then the command effects this current object
            float discriminant = (b * b) - (4 * a * c);
            if (discriminant >= 0)
            {
                over = true;
            }
            else
            {
                over = false;
            }

            //send mode, mark target for later processing
            if (over & send)
            {
                input = pInput[0];
                target = planet[i];
                input.target = target;
                input.targetStar = system[i].star;
                input.send = 1;
                pInput[0] = input;
            }

            //mark this object as selected
            if (over & select)
            {
                if (_order.selected == 0)
                {
                    _order.selected = 1;
                }
                else
                {
                    _order.selected = 0;
                }
                orders[i] = _order;
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        //the modes to send into the job
        bool select = false;
        bool send = false;

        if (Input.GetMouseButtonDown(1))
        {
            //right click
            //i am issueing a send command
            select = false;
            send = true;

        } else if (Input.GetMouseButtonDown(0))
        {
            //left click
            //issue a select command
            select = true;
            send = false;
        }

        //if im either selecting or sending i need to do the job
        if (select || send)
        {
            //create a ray and send it to the job for processing
            Ray ray;
            Vector3 pos = Input.mousePosition;
            ray = WorldManager.data.cam.ScreenPointToRay(pos);

            PlayerSelectJob PlayerSelectJob = new PlayerSelectJob
            {
                planet = _planets.Planet,
                pos = _planets.pos,
                orb = _planets.orb,
                system=_planets.system,
                team = _planets.team,
                orders = _planets.orders,

                rayDirection = ray.direction,
                rayOrigin = ray.origin,
                select = select,
                send = send,
                pInput = pInput.pInput,
            };
            PlayerSelectHandle = PlayerSelectJob.Schedule(_planets.Length,1, inputDeps);
        }

        return PlayerSelectHandle;
    }
}