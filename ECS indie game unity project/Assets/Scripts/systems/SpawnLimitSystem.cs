﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

[UpdateAfter(typeof(PlayerSelectSystem))]
public class SpawnLimitSystem : JobComponentSystem
{

    //calculated how many ships should be spawned per faction

    public static JobHandle spawnLimitHandle;

    struct Ships
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<Orbiting> orbiting;
        [ReadOnly] public ComponentDataArray<Team> shipTeam;
        [ReadOnly] public ComponentDataArray<Attack> attack;
    }

    struct PlanetsData
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<Orbitable> orbitable;
        [ReadOnly] public ComponentDataArray<Team> team;
    }

    [Inject] private PlanetsData planetsData;
    [Inject] private Ships _ships;

    [BurstCompile]
    struct SpawnLimitJob : IJobProcessComponentData<PlayerInput,Team>
    {
        [ReadOnly] public ComponentDataArray<Orbitable> planetOrbitable;
        [ReadOnly] public ComponentDataArray<Team> planetTeam;
        public int planetArrayLength;

        [ReadOnly] public ComponentDataArray<Orbiting> orbiting;
        [ReadOnly] public ComponentDataArray<Team> shipTeam;
        public int shipLength;

        private int spawnLimit;

        //for each input
        public void Execute(ref PlayerInput input, [ReadOnly] ref Team team)
        {
            spawnLimit = 0;
            //foreach planet
            for( int i=0; i<planetArrayLength; i++)
            {
                //if it has been captured
                if (planetOrbitable[i].built==1) {
                    //on the same team
                    if (planetTeam[i].id == team.id)
                    {
                        spawnLimit += planetOrbitable[i].supplySize;
                    }
                }
            }
            input.SpawnLimit = spawnLimit;

            //total number of units owned by this input
            int currentUnits=0;
            for(int j=0;j<shipLength;j++)
            {
                if(team.id==shipTeam[j].id)
                {
                    currentUnits++;
                }
            }
            input.currentUnits = currentUnits;
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        JobHandle newHandle = JobHandle.CombineDependencies(inputDeps, PlayerSelectSystem.PlayerSelectHandle);

        SpawnLimitJob spawnLimitJob = new SpawnLimitJob
        {
            planetOrbitable = planetsData.orbitable,
            planetTeam = planetsData.team,
            orbiting = _ships.orbiting,
            shipTeam = _ships.shipTeam,
            shipLength = _ships.Length,
            planetArrayLength = planetsData.Length,
        };
        spawnLimitHandle = spawnLimitJob.Schedule(this, newHandle);

        return spawnLimitHandle;
    }
}
