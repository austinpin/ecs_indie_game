﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(SpawnerSystem))]
public class SpawnerBarrier : BarrierSystem { }

[UpdateAfter(typeof(SpawnLimitSystem))]
[UpdateAfter(typeof(PlayerSelectSystem))]
[UpdateAfter(typeof(DeathSystem))]
public class SpawnerSystem : JobComponentSystem
{

    //calculates how many ships to spawn

    public static JobHandle spawnerHandle;
    
    struct Planets
    {
        public readonly int Length;
        public EntityArray PlanetE;
        [ReadOnly] public ComponentDataArray<Team> team;
        [ReadOnly] public ComponentDataArray<Position> position;
        public ComponentDataArray<Orbitable> orbitable;
        [ReadOnly] public ComponentDataArray<UnitSpawner> spawner;
    }

    [Inject] private Planets planets;
    [Inject] private SpawnerBarrier spawnerBarrier;

    [BurstCompile]
    private struct SpawnDelayJob : IJobProcessComponentData<UnitSpawner>
    {
        public float deltaTime;

        public void Execute(ref UnitSpawner spawner)
        {
            spawner.CurrentDelay -= deltaTime;
        }
    }

        //[BurstCompile]
    private struct SpawnJob : IJobProcessComponentData<Team, PlayerInput>
    {
        [ReadOnly] public EntityCommandBuffer buffer;

        public int Length;
        public EntityArray PlanetE;
        [ReadOnly] public ComponentDataArray<Team> _team;
        [ReadOnly] public ComponentDataArray<Position> position;
        [ReadOnly] public ComponentDataArray<UnitSpawner> spawner;

        //public Unity.Mathematics.Random rand;

        //for each input
        public void Execute([ReadOnly] ref Team team, ref PlayerInput input)
        {
            int ammountToSpawn;
            int missingCount;
            int unitsToSpawn=0;

            //for all planets
            for (int i = 0; i < Length; i++)
            {
                //if its on the same team as the input
                if (_team[i].id == team.id)
                {
                    UnitSpawner _spawner = spawner[i];
                    _spawner.ammountCurrent = 0;
                    //if its ready to spawn
                    if (_spawner.CurrentDelay <= 0)
                    {
                        //reset delay
                        _spawner.CurrentDelay = _spawner.MaxDelay;
                        
                        //check if we need to spawn, using input
                        missingCount = 0;
                        missingCount = input.SpawnLimit - input.currentUnits;
                        input.missingUnits = missingCount;
                        //if we actually need to spawn
                        if (missingCount > 0)
                        {

                            if (missingCount < _spawner.ammountPerTick)
                            {
                                ammountToSpawn = missingCount; //spawn all we can
                            }
                            else
                            {
                                ammountToSpawn = _spawner.ammountPerTick; //spawn as per our rating
                            }

                            _spawner.ammountCurrent = ammountToSpawn; //how many this one will spawn
                            input.currentUnits += ammountToSpawn;
                            unitsToSpawn += ammountToSpawn; //next iteration units
                            

                            if (input.currentUnits >= input.SpawnLimit)
                            {
                                buffer.SetComponent(PlanetE[i], _spawner);
                                break;
                            }
                        }
                    }
                    buffer.SetComponent(PlanetE[i], _spawner);
                }
            }

            input.spawnBatch =unitsToSpawn; //how many this input will spawn
        }
    }


    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        uint seed = (uint) UnityEngine.Random.Range(1, 100000);

        //reduce its counter by time
        SpawnDelayJob spawnDelayJob = new SpawnDelayJob
        {
            deltaTime=Time.deltaTime,
        };
        JobHandle newHandle = spawnDelayJob.Schedule(this, inputDeps);

        //check if its ready to spawn
        SpawnJob spawnJob = new SpawnJob
        {
            Length = planets.Length,
            PlanetE = planets.PlanetE,
            _team = planets.team,
            position = planets.position,
            spawner = planets.spawner,

            buffer = spawnerBarrier.CreateCommandBuffer(),
            //rand = new Unity.Mathematics.Random(seed),
        };
        spawnerHandle = spawnJob.Schedule(this, newHandle);

        return spawnerHandle;
    }
}
