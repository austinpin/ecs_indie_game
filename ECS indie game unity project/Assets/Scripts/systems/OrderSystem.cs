﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

[UpdateAfter(typeof(OrderSystem))]
public class OrderBarrier : BarrierSystem { }

[UpdateAfter(typeof(PlayerSelectSystem))]
[UpdateAfter(typeof(SpawnLimitSystem))]
public class OrderSystem : JobComponentSystem
{

    //this system carries on from player select system and gives the ships their initial directions
    //allowing them to move

    public static JobHandle orderHandle;

    struct OrdersData
    {
        public readonly int Length;
        public EntityArray orbitEntity;
        [ReadOnly] public ComponentDataArray<Orders> orders;
        [ReadOnly] public SharedComponentDataArray<StarSystem> system;
        [ReadOnly] public ComponentDataArray<Orbitable> orbitable;
        [ReadOnly] public ComponentDataArray<Team> team;
    }

    struct Ships
    {
        public readonly int Length;
        public EntityArray Ship;
        [ReadOnly] public ComponentDataArray<HyperTravel> hyper;
        [ReadOnly] public ComponentDataArray<Position> position;
        [ReadOnly] public ComponentDataArray<ParentE> parent;
        [ReadOnly] public ComponentDataArray<Orbiting> orbiting;
        [ReadOnly] public ComponentDataArray<Team> team;
    }

    private struct StarSystems
    {
        public readonly int Length;
        [ReadOnly] public SharedComponentDataArray<StarSystem> starSystem;
        [ReadOnly] public SharedComponentDataArray<StarThrough> starThrough;
        [ReadOnly] public SharedComponentDataArray<StarDestination> starDestination;
    }

    private struct StarGroups
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<StarGroup> groupArray;
    }

    [Inject] StarSystems systems;
    [Inject] StarGroups groups;

    [Inject] private OrdersData ordersData;
    [Inject] private Ships _ships;
    [Inject] private OrderBarrier orderBarrier;

    [Inject]
    [ReadOnly]
    ComponentDataFromEntity<Position> allPos;

    //[BurstCompile]
    private struct OrderJob : IJobProcessComponentData<PlayerInput, Team>
    {
        public int ordersLength;
        public EntityArray orbitEntity;
        [ReadOnly] public ComponentDataArray<Orders> orders;
        [ReadOnly] public SharedComponentDataArray<StarSystem> system;
        [ReadOnly] public ComponentDataArray<Orbitable> orbitable;
        [ReadOnly] public ComponentDataArray<Team> orderTeam;

        public int shipsLength;
        public EntityArray ship;
        [ReadOnly] public ComponentDataArray<HyperTravel> hyper;
        [ReadOnly] public ComponentDataArray<Position> position;
        [ReadOnly] public ComponentDataArray<ParentE> parent;
        [ReadOnly] public ComponentDataArray<Orbiting> orbiting;
        [ReadOnly] public ComponentDataArray<Team> team;

        public int pathfinderLength;
        [ReadOnly] public SharedComponentDataArray<StarSystem> starSystem;
        [ReadOnly] public SharedComponentDataArray<StarThrough> starThrough;
        [ReadOnly] public SharedComponentDataArray<StarDestination> starDestination;
        public int groupLength;
        [ReadOnly] public ComponentDataArray<StarGroup> groupArray;

        [ReadOnly]
        public ComponentDataFromEntity<Position> allPos;

        [ReadOnly]
        public EntityCommandBuffer buffer;

        //we are checking every input
        public void Execute(ref PlayerInput myInput, [ReadOnly] ref Team myTeam)
        {
            Orders _order;
            Entity orbitE;
            Entity newTarget;

            //read this indent cascade in order to see what the words say
            //(1)if this input is sending
            if (myInput.send==1) {
                
                newTarget = myInput.target;
                //(2)check everyplanet
                for (int j = 0; j < ordersLength; j++)
                {
                    // current planet entity
                    orbitE = orbitEntity[j];

                    //(3)for orders and if its selected
                    _order = orders[j];
                    if (_order.selected == 1)
                    {
                        //dijkstra's algorithm baked version
                        //place here for pathfinding through starlanes
                        //see that script for more information
                        //we know what planet this is and we know where the input wants to go, so the path finder tells us where to go next
                        for (int a = 0; a < pathfinderLength; a++)
                        {
                            if (starDestination[a].star == myInput.targetStar)
                            {
                                if (starSystem[a].star == system[j].star)
                                {
                                    newTarget = starThrough[a].star;
                                }
                            }
                        }

                        //(4)foreach ship (for all of the ships)
                        for (int i = 0; i < shipsLength; i++)
                        {
                            //ships parent entity
                            ParentE parentRef = parent[i];

                            //(5)if its orbiting this planet and the ship is on the same team as the input
                            //issue the command to this ship
                            if (parentRef.entity == orbitE && team[i].id == myTeam.id)
                            {
                                HyperTravel hyperT = hyper[i];

                                if (hyperT.cooldown<=0) {
                                    hyperT.cooldown = hyperT.maxCooldown;
                                    Position posi = position[i];
                                    Orbiting orbi = orbiting[i];
                                    Orbitable _orbitable = orbitable[j];

                                    //set up the hyper travel
                                    hyperT.finalDestination = myInput.target;
                                    hyperT.finalStar = myInput.targetStar;
                                    hyperT.target = newTarget;
                                    hyperT.active = 1;

                                    //ensure the orbiting radius is setup such that this ship orbits the new planet at a new radius and travels to that spot
                                    float2 radiusOffset = new float2(0, _orbitable.radius.y - _orbitable.radius.x); //relative radius from 0
                                    float myRadiusOffset = orbi.myRadius - _orbitable.radius.x; //radius of this ship between them
                                    hyperT.offset = math.normalize(posi.Value - orbi.parentPos); //direction from planet
                                    hyperT.percentOffset = math.unlerp(radiusOffset.x, radiusOffset.y, myRadiusOffset);

                                    //disable orbiting
                                    orbi.active = 0;
                                    parentRef.entity = Entity.Null;

                                    //tell the buffer to assign the components to this entity
                                    buffer.SetComponent(ship[i], hyperT);
                                    buffer.SetComponent(ship[i], orbi);
                                    buffer.SetComponent(ship[i], parentRef);
                                }

                                /*
                                hyper[i] = hyperT;
                                orbiting[i] = orbi;
                                parent[i] = parentRef;
                                */

                                //break;
                            }
                        }
                    }
                }
                //we are no longer sending
                myInput.send = 0;
                myInput.target = Entity.Null;
                //set order selected to 0
                for (int k = 0; k < ordersLength; k++)
                {
                    if (orderTeam[k].id==myTeam.id)
                    {
                        _order = orders[k];
                        _order.selected = 0;
                        buffer.SetComponent(orbitEntity[k], _order);
                    }
                }
            }

        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        //use component group data extraction to find target system or
        //use loop through, find current system, loop through find desination system, thus should be only 1 entry for target

        OrderJob orderJob = new OrderJob
        {
            orders = ordersData.orders,
            ordersLength = ordersData.Length,
            orbitable = ordersData.orbitable,
            system=ordersData.system,
            orbitEntity = ordersData.orbitEntity,
            orderTeam= ordersData.team,

            ship = _ships.Ship,
            hyper = _ships.hyper,
            position = _ships.position,
            parent = _ships.parent,
            orbiting = _ships.orbiting,
            team = _ships.team,

            pathfinderLength=systems.Length,
            starSystem=systems.starSystem,
            starThrough=systems.starThrough,
            starDestination=systems.starDestination,

            groupLength=groups.Length,
            groupArray=groups.groupArray,

            shipsLength = _ships.Length,
            allPos = allPos,
            buffer = orderBarrier.CreateCommandBuffer(),
        };
        orderHandle = orderJob.Schedule(this, inputDeps);

        return orderHandle;
    }
}
