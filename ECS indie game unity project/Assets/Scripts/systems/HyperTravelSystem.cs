﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

[UpdateAfter(typeof(OrderSystem))]
[UpdateAfter(typeof(OrderBarrier))]
public class HyperTravelSystem : JobComponentSystem
{

    //this system moves ships in hypertravel mode to their destination
    //ships are then marked for arrival once they are at the destination

    public static JobHandle shiptravelHandle;


    //this is a different way of finding what you need,
    //by just grabbing it directly, it should be faster than checking everything if its correct
    //i dont know why its not implemented everything but first time using ECS difficulties, cant know everything on the first day
    [Inject]
    [ReadOnly]
    ComponentDataFromEntity<Position> pos;

    [Inject]
    [ReadOnly]
    ComponentDataFromEntity<Orbitable> orbitable;

    private struct ShipSetArrivalJob : IJobProcessComponentData<HyperTravel>
    {
        public float deltaTime;
        [ReadOnly] public ComponentDataFromEntity<Position> posArray;
        [ReadOnly] public ComponentDataFromEntity<Orbitable> orbitableArray;

        public void Execute(ref HyperTravel hyper)
        {
            if (hyper.active == 1)
            {
                Entity target = hyper.target;
                Position newPos = posArray[target]; //extract position from target
                Orbitable orb = orbitableArray[target]; //extract orbitable from target

                hyper.targetRadius = math.lerp(orb.radius.x, orb.radius.y, hyper.percentOffset); //find target radius from data
                hyper.arrivalPos = newPos.Value+(hyper.offset*hyper.targetRadius); //set arrival position from target
                
            }
            if (hyper.active == 0)
            {
                hyper.cooldown -= deltaTime;
            }
        }
    }

    [BurstCompile]
    private struct ShipTravelJob : IJobProcessComponentData<HyperTravel, Position>
    {
        public float deltaTime;

        public void Execute(ref HyperTravel hyper, ref Position pos)
        {
            if (hyper.active == 1)
            {
                float3 direction = hyper.arrivalPos - pos.Value; //distance to planet also
                float3 directionNorm = math.normalize(direction);
                float3 movement = directionNorm * deltaTime * hyper.speed;
                if (math.length(movement)<math.length(direction)) //are we close to the planet
                {
                    pos.Value += movement; //move ship
                }
                else
                {
                    pos.Value = hyper.arrivalPos; 
                    hyper.arrived = 1;
                }
                
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        //first the position of the target might have moved so find out where our target is
        ShipSetArrivalJob shipSetArrivalJob = new ShipSetArrivalJob
        {
            deltaTime=Time.deltaTime,
            posArray = pos,
            orbitableArray = orbitable,
        };
        JobHandle shipSetArriveaHandle = shipSetArrivalJob.Schedule(this, inputDeps);

        //go to that target
        ShipTravelJob shipTravelJob = new ShipTravelJob
        {
            deltaTime = Time.deltaTime,
        };
        shiptravelHandle = shipTravelJob.Schedule(this, shipSetArriveaHandle);

        return shiptravelHandle;
    }
}
