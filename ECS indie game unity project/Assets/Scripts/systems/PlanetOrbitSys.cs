﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

public class PlanetOrbitSys : JobComponentSystem
{

    //the same as the ordinary ship orbit system but for planets
    //its breaking some programming rules
    //but it was not behaving properly so a seperate system had to be made

    public static JobHandle planetOrbitHandle;

    // Start is called before the first frame update
    void Start()
    {

    }

    struct PlanetsData
    {
        public readonly int Length;
        public EntityArray planetE;
        [ReadOnly] public ComponentDataArray<Orbitable> orbitable;
        //[ReadOnly] public ComponentDataArray<ID> id;
        [ReadOnly] public ComponentDataArray<Position> position;
    }

    [Inject] PlanetsData planets;

    [BurstCompile]
    private struct SetOrbitJob : IJobProcessComponentData<PlanetOrbit, ParentE>
    {

        public int length;
        public EntityArray planetE;
        //[ReadOnly] public ComponentDataArray<ID> id;
        [ReadOnly] public ComponentDataArray<Position> position;

        public void Execute(ref PlanetOrbit orbit, [ReadOnly] ref ParentE parent)
        {
            for (int i = 0; i < length; i++)
            {
                if (/*parent.id == id[i].id*/ parent.entity == planetE[i])
                {
                    orbit.parentPos = position[i].Value;
                    break;
                }
            }
        }
    }

    [BurstCompile]
    private struct OrbitJob : IJobProcessComponentData<Position, PlanetOrbit>
    {
        public float deltaTime;

        public void Execute(ref Position pos, ref PlanetOrbit orbit)
        {

            //orbit.tilt += deltaTime * orbit.orbitSpeed * 0.25f;
            orbit.rotation += deltaTime * orbit.orbitSpeed;

            float3 _pos = pos.Value;
            _pos.x = (math.sin(orbit.rotation) * (math.cos(orbit.tilt)) * orbit.radius) + orbit.parentPos.x;
            _pos.y = (math.sin(orbit.rotation) * (math.sin(orbit.tilt)) * orbit.radius) + orbit.parentPos.y;
            _pos.z = (math.cos(orbit.rotation) * orbit.radius) + orbit.parentPos.z;
            pos.Value = _pos;
        }
    }


    // Update is called once per frame
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        SetOrbitJob setOrbitJob = new SetOrbitJob
        {
            length = planets.Length,
            planetE = planets.planetE,
            //id = planets.id,
            position=planets.position,
        };
        JobHandle newHandle = setOrbitJob.Schedule(this, inputDeps);

        OrbitJob orbitJob = new OrbitJob
        {
            deltaTime = Time.deltaTime,
        };
        planetOrbitHandle = orbitJob.Schedule(this, newHandle);

        return planetOrbitHandle;
    }
}
