﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

[UpdateAfter(typeof(ArrivalSystem))]
[UpdateAfter(typeof(PlanetOrbitSys))]
public class OrbitSystem : JobComponentSystem
{

    //moves ships around their planet

    public static JobHandle orbitHandle;

    struct PlanetsData
    {
        public readonly int Length;
        public EntityArray planetE;
        [ReadOnly] public ComponentDataArray<Orbitable> orbitable;
        [ReadOnly] public ComponentDataArray<Position> position;
    }

    [Inject] PlanetsData planets;

    [BurstCompile]
    private struct SetOrbitJob : IJobProcessComponentData<Orbiting, ParentE>
    {

        public int length;
        public EntityArray planetE;
        [ReadOnly] public ComponentDataArray<Position> position;

        public void Execute( ref Orbiting orbiting,[ReadOnly] ref ParentE parent)
        {
            if (orbiting.active == 1)
            {
                for (int i=0;i<length;i++)
                {
                    if (parent.entity==planetE[i])
                    {
                        orbiting.parentPos = position[i].Value;
                        break;
                    }
                }
                
            }
        }
    }

    [BurstCompile(Accuracy.Low,Support.Relaxed)]
    private struct OrbitJob : IJobProcessComponentData<Position, Orbiting, Rotation>
    {
        public float deltaTime;

        public void Execute(ref Position pos, ref Orbiting orbiting, ref Rotation rot)
        {
            if (orbiting.active==1) {

                orbiting.tilt += deltaTime*orbiting.orbitSpeed * 0.25f;
                orbiting.rotation += deltaTime*orbiting.orbitSpeed;

                float3 _pos = pos.Value;
                _pos.x = (math.sin(orbiting.rotation) * (math.cos(orbiting.tilt)) * orbiting.myRadius) + orbiting.parentPos.x;
                _pos.y = (math.sin(orbiting.rotation) * (math.sin(orbiting.tilt)) * orbiting.myRadius) + orbiting.parentPos.y;
                _pos.z = (math.cos(orbiting.rotation) * orbiting.myRadius) + orbiting.parentPos.z;
                pos.Value = _pos;
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        JobHandle newHandle = JobHandle.CombineDependencies(ArrivalSystem.arrivalHandle, inputDeps);

        //follow parent posision
        SetOrbitJob setOrbitJob = new SetOrbitJob
        {
            length = planets.Length,
            planetE = planets.planetE,
            position = planets.position,
        };
        JobHandle newHandle2 = setOrbitJob.Schedule(this, newHandle);

        //orbit
        OrbitJob orbitJob = new OrbitJob
        {
            deltaTime = Time.deltaTime,
        };
        orbitHandle = orbitJob.Schedule(this, newHandle2);
        return orbitHandle;
    }
}
