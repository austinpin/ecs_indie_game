﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

//public class AttackBarrier : BarrierSystem { }

[UpdateAfter(typeof(OrderSystem))]
[UpdateAfter(typeof(OrderBarrier))]
//[UpdateAfter(typeof(UtilitiesSystem))]
public class ShipAttackSystem : JobComponentSystem
{

    //allows ships to attack other ships on their node

    public static JobHandle attackHandle;
    //public Unity.Mathematics.Random random=new Unity.Mathematics.Random(6);
    
    struct Ships
    {
        public readonly int Length;
        public EntityArray shipE;
        [ReadOnly] public ComponentDataArray<ParentE> parent;
        public ComponentDataArray<Health> health;
        public ComponentDataArray<Attack> attack;
        [ReadOnly] public ComponentDataArray<Team> shipTeam;
    }

    struct Planets
    {
        public readonly int Length;
        public EntityArray planetE;
        [ReadOnly] public ComponentDataArray<Planet> planet;
    }

    [Inject] private Ships _ships;
    //[Inject] private Planets planets;
    //[Inject] private AttackBarrier attackBarrier;

    
    [BurstCompile]
    private struct ShipDelayJob :IJobProcessComponentData <Attack>
    {
        public float deltaTime;
        public void Execute(ref Attack attack)
        {
            attack.currentDelay -= deltaTime;
        }
    }



    [BurstCompile]
    private struct AttackJob : IJobProcessComponentDataWithEntity<Planet>
    {
        
        public int shipsLength;
        public EntityArray shipE;
        [ReadOnly] public ComponentDataArray<ParentE> shipsParent;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Health> shipsHealth;
        [NativeDisableParallelForRestriction] public ComponentDataArray<Attack> shipsAttack;
        [ReadOnly] public ComponentDataArray<Team> shipsTeam;

        //[ReadOnly] public EntityCommandBuffer buffer;

        //public EntityArray planetE;
        //[ReadOnly] public SharedComponentDataArray<Planet> _planet;

        public void Execute(Entity self,int index,ref Planet planet)
        {
            planet.battle = 0;
            int team = 0;
            //check if there is a battle (if there are 2 different teams here)
            for (int h = 0; h < shipsLength; h++)
            {
                Entity parentEntity = shipsParent[h].entity;
                bool check = self == parentEntity;

                if (check)
                {
                    if (team == 0)
                    {
                        team = shipsTeam[h].id;
                    }
                    else if (team != shipsTeam[h].id)
                    {
                        planet.battle = 1;
                        break;
                    }

                }

            }

            if (planet.battle == 1)
            {
                int currentTeam;
                int currentTeamTotalDamage;

                //for each team up to 10
                for (int k=0; k<10;k++)
                {
                    currentTeamTotalDamage = 0;
                    currentTeam = k;
                    //for each ship
                    for (int l = 0; l< shipsLength; l++)
                    {
                        //oribiting me
                        if (self==shipsParent[l].entity) {
                            if (currentTeam == shipsTeam[l].id)
                            {
                                Attack shipAttack = shipsAttack[l];
                                if (shipAttack.currentDelay<=0) {
                                    currentTeamTotalDamage += shipsAttack[l].damage; //store the damamge of all the ships current
                                    shipAttack.currentDelay = shipAttack.maxDelay;
                                    //buffer.SetComponent(shipE[l], shipAttack);
                                    shipsAttack[l] = shipAttack;
                                }
                            }
                        }
                    }

                    //use the stored damage to deal damage
                    if (currentTeamTotalDamage>0) {
                        for (int m = 0; m < shipsLength; m++)
                        {
                            if (self == shipsParent[m].entity) {
                                if (currentTeam == shipsTeam[m].id)
                                {

                                }
                                else
                                {
                                    Health health = shipsHealth[m];
                                    if (health.health >= currentTeamTotalDamage)
                                    {
                                        health.health -= (byte) currentTeamTotalDamage;
                                        currentTeamTotalDamage = 0;
                                    }
                                    else
                                    {
                                        currentTeamTotalDamage -= health.health;
                                        health.health = 0;
                                    }
                                    //buffer.SetComponent(shipE[m], health);
                                    shipsHealth[m] = health;
                                    if (currentTeamTotalDamage <= 0)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        //set attack delay
        ShipDelayJob delayjob = new ShipDelayJob
        {
            deltaTime = Time.deltaTime,
        };
        JobHandle newHandle = delayjob.Schedule(this, inputDeps);
        
        
        
        JobHandle newHandle2 = JobHandle.CombineDependencies(newHandle, OrderSystem.orderHandle);
        AttackJob attackJob = new AttackJob
        {
            
            shipsLength = _ships.Length,
            shipE = _ships.shipE,
            shipsParent = _ships.parent,
            shipsHealth = _ships.health,
            shipsAttack = _ships.attack,
            shipsTeam = _ships.shipTeam,
            
            //planetE=planets.planetE,
            //_planet=planets.planet,

            //buffer = attackBarrier.CreateCommandBuffer(),
        };
        attackHandle = attackJob.Schedule(this, newHandle2);
        

        return attackHandle;
        //return newHandle;
    }
}