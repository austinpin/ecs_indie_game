﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

[UpdateAfter(typeof(SpawnLimitSystem))]
public class ArrivalSystem : JobComponentSystem
{

    //once a ship is marked for arrival
    //it should orbit the planet

    public static JobHandle arrivalHandle;
   
    [BurstCompile]
    private struct ArrivalJob : IJobProcessComponentData<Orbiting, HyperTravel,ParentE>
    {

        public void Execute(ref Orbiting orbiting, ref HyperTravel hyper, ref ParentE parent)
        {
            if (hyper.arrived == 1)
            {
                hyper.arrived = 0;
                hyper.active = 0;
                orbiting.active = 1;
                orbiting.myRadius = hyper.targetRadius;
                parent.entity = hyper.target;
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        JobHandle newHandle = JobHandle.CombineDependencies(inputDeps, SpawnLimitSystem.spawnLimitHandle);

        ArrivalJob arrivalJob = new ArrivalJob
        {
        };
        arrivalHandle = arrivalJob.Schedule(this, newHandle);
        return arrivalHandle;
    }
}
