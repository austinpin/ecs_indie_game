﻿using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Unity.Burst;

[UpdateAfter(typeof(PlayerSelectSystem))]
public class HighlightSys : ComponentSystem
{

    //allows the player to know what they have selected

    // Start is called before the first frame update
    void Start()
    {

    }

    struct highlightE
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<HighlightTag> highlight;
    }

    [Inject] highlightE _highlightE;

    struct OrdersArray
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<Orders> orders;
        public ComponentDataArray<Position> positon;
    }

    [Inject] OrdersArray _orders;


    // Update is called once per frame
    protected override void OnUpdate()
    {
        //copy data as we are dynamically spawning entities
        NativeArray<Entity> highlightE = new NativeArray<Entity>(_highlightE.Length, Allocator.Temp);
        NativeArray<HighlightTag> highlight = new NativeArray<HighlightTag>(_highlightE.Length, Allocator.Temp);

        NativeArray<Entity> orderE = new NativeArray<Entity>(_orders.Length, Allocator.Temp);
        NativeArray<Orders> orders = new NativeArray<Orders>(_orders.Length, Allocator.Temp);
        NativeArray<Position> position = new NativeArray<Position>(_orders.Length, Allocator.Temp);

        _highlightE.self.CopyTo(highlightE, 0);
        _highlightE.highlight.CopyTo(highlight, 0);

        _orders.self.CopyTo(orderE, 0);
        _orders.orders.CopyTo(orders, 0);
        _orders.positon.CopyTo(position, 0);

        for (int i = 0; i< _orders.Length;i++)
        {
            Orders current = orders[i];
            if (current.selected==1)
            {
                if (current.highlightE == Entity.Null)
                {
                    //create entity set data
                    Entity highlightSelf = EntityManager.CreateEntity(Data.highlightE);
                    EntityManager.SetComponentData(highlightSelf, new HighlightTag { target = orderE[i] });
                    EntityManager.SetSharedComponentData(highlightSelf, new MeshInstanceRenderer { mesh = WorldManager.data.highlightMesh, material = WorldManager.data.highlightMaterial });

                    //set planets entity data value
                    current.highlightE = highlightSelf;
                    EntityManager.SetComponentData(orderE[i], current);

                    //set position of highlight
                    Position pos;
                    pos.Value = position[i].Value + new float3(0, 10, 0);
                    EntityManager.SetComponentData(current.highlightE, pos);
                }
            } else if (current.selected == 0)
            {
                if (current.highlightE != Entity.Null) {
                    EntityManager.DestroyEntity(current.highlightE);
                    current.highlightE = Entity.Null;
                    EntityManager.SetComponentData(orderE[i], current);
                }
            }

            //highlight should follow planet
            if (current.selected == 1 && current.highlightE != Entity.Null)
            {
                Position pos;
                pos.Value = position[i].Value + new float3(0,10,0);
                EntityManager.SetComponentData(current.highlightE, pos);
            }
        }
    }
}
