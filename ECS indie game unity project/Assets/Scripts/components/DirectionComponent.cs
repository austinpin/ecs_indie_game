﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

[System.Serializable]
public struct Direction : IComponentData
{

    public float3 direction;

}
public class DirectionComponent : ComponentDataWrapper<Direction> { }