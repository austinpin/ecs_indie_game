﻿using Unity.Entities;
using UnityEngine;

[System.Serializable]
public struct Health : IComponentData
{
    public byte health;
}
public class HealthComponent : ComponentDataWrapper<Health> { }
