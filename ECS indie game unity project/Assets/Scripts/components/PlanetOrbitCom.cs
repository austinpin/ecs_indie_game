﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using System.Collections.Generic;

[System.Serializable]
public struct PlanetOrbit : IComponentData
{
    public float3 parentPos;
    public float radius;
    public float tilt;
    public float rotation;

    public float orbitSpeed;
}
public class PlanetOrbitCom : ComponentDataWrapper<PlanetOrbit> { }