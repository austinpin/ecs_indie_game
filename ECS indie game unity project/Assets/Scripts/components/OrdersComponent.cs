﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

[System.Serializable]
public struct Orders : IComponentData
{
    public byte selected;
    public Entity highlightE;
}
public class OrdersComponent : ComponentDataWrapper<Orders> { }