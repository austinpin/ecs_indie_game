﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

[System.Serializable]
public struct Orbiting : IComponentData
{
    public byte active;
    public float3 parentPos;
    public float myRadius;
    public float orbitSpeed;
    public float rotation;
    public float tilt;
}
public class OrbitingComponent : ComponentDataWrapper<Orbiting> { }