﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using System.Collections.Generic;

[System.Serializable]
public struct StarConnector : IComponentData
{
    public Entity target1;
    public Entity target2;

    //the weight of this connection for pathfinding
    public float myDistance;
}
public class StarConnectorCom : ComponentDataWrapper<StarConnector> { }