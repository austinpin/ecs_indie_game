﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using System.Collections.Generic;

[System.Serializable]
public struct StarGroup : IComponentData
{
    public Entity star;
    public byte visted;
    public float stackedDistance;
    public Entity shortestDistanceLastStar;
}
public class StarGroupCom : ComponentDataWrapper<StarGroup> { }