﻿using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct Orbitable : IComponentData
{
    public float2 radius; //min //max
    public Entity self;
    public float captureProgress;
    public float captureLimit;
    public byte built; //actully captured

    public int numberOfOrbitingShips;
    public int supplySize;

    public int teamIDStore;
}
public class OrbitableComponent : ComponentDataWrapper<Orbitable> { }