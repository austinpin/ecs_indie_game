﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[System.Serializable]
public struct UnitSpawner : IComponentData
{
    public int ammountPerTick;
    public int ammountCurrent;

    public float CurrentDelay;
    public float MaxDelay;
}
public class UnitSpawnerComponent : ComponentDataWrapper<UnitSpawner> { }
