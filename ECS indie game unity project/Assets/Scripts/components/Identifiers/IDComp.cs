﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using System.Collections.Generic;

[System.Serializable]
public struct ID : IComponentData
{
    public byte id;
}
public class IDComp : ComponentDataWrapper<ID> { }