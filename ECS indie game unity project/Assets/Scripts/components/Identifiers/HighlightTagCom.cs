﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using System.Collections.Generic;

[System.Serializable]
public struct HighlightTag : IComponentData
{
    public Entity target;
}
public class HighlightTagCom : ComponentDataWrapper<HighlightTag> { }