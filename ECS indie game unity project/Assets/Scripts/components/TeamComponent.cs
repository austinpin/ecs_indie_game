﻿using Unity.Entities;

[System.Serializable]
public struct Team : IComponentData
{
    public int id;
}
public class TeamComponent : ComponentDataWrapper<Team> { }