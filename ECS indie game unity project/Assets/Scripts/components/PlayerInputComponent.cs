﻿using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct PlayerInput : IComponentData
{
    public Entity target;
    public Entity targetStar;
    public byte send;

    public int currentUnits;
    public int SpawnLimit;
    public int missingUnits;
    public int spawnBatch;
}
public class PlayerInputComponent : ComponentDataWrapper<PlayerInput> { }
