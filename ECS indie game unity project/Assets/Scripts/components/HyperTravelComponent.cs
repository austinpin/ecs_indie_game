﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;
using System.Collections.Generic;

[System.Serializable]
public struct HyperTravel : IComponentData
{
    public byte active;
    public float speed;

    public Entity finalDestination;
    public Entity finalStar;
    public Entity target;

    public float cooldown;
    public float maxCooldown;

    public float3 arrivalPos;
    public float targetRadius;
    public float3 offset;
    public float percentOffset;
    public byte arrived;
}
public class HyperTravelComponent : ComponentDataWrapper<HyperTravel> { }