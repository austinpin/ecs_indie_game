﻿using Unity.Entities;

[System.Serializable]
public struct ParentE : IComponentData
{
    public Entity entity;
    //public byte id;
}
public class ParentECom : ComponentDataWrapper<ParentE> { }
