﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Rendering;

[System.Serializable]
public struct UnitSpawnData : ISharedComponentData {

    public Position pos;
    public Rotation rot;
    public ParentE owner;
    public Health health;
    public Orbiting orbiting;
    public HyperTravel hyper;
    public MeshInstanceRenderer rend;
    public Team team;
    public Attack attack;
}
public class UnitSpawnDataComponent : SharedComponentDataWrapper<UnitSpawnData> { }