﻿using Unity.Entities;

[System.Serializable]
public struct Attack : IComponentData
{
    public byte damage;
    public float currentDelay;
    public float maxDelay;
}
public class AttackComponent : ComponentDataWrapper<Attack> { }
