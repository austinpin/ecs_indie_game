﻿using Unity.Entities;
using Unity.Collections;

[System.Serializable]
public struct Planet : IComponentData
{
    public byte battle;
}
public class PlanetComponent : ComponentDataWrapper<Planet> { }